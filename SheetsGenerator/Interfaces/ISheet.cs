﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SheetsGenerator.Sheets;

namespace SheetsGenerator.Interfaces
{
    public interface ISheet
    {
        byte[] GenerateSheet();
        string GetReportFileName();
    }

    public enum SheetsTypes
    {
        UniSheet,
        ContactSheet
    }
}
