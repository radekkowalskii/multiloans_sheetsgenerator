﻿using SheetsGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheetsGenerator.Sheets
{
    class UniSheet : ISheet
    {
        const string FILE_NAME = "UniSheet.rdlc";

        public byte[] GenerateSheet()
        {
            Generator generator = new Generator();
            return generator.PrintSheet(this);
        }

        public SheetsTypes GetSheetType()
        {
            return SheetsTypes.UniSheet;
        }

        public string GetReportFileName()
        {
            //aa1.mytableDataTable dt = new aa1.mytableDataTable();
            //var xy = dt.Select(x => x.firstname.Equals("")).ToList();
            //dt.AcceptChanges();

            //foreach (var item in xy)
            //{

            //}
            return FILE_NAME;
        }
    }
}
