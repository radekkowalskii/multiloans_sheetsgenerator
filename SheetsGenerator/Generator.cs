﻿using Microsoft.Reporting.WebForms;
using Npgsql;
using SheetsGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SheetsGenerator
{
    class Generator
    {
        internal byte[] PrintSheet(ISheet sheet)
        {
            string report_name = sheet.GetReportFileName();
            //string report_name = "Report2.rdlc";
            Warning[] warnings;
            string[] streamids;
            string mimeType, encoding, extension = string.Empty;
            string deviceInfo = @"
                  <DeviceInfo>
                    <OutputFormat>PDF</OutputFormat>
                    <PageWidth>8.5in</PageWidth>
                    <PageHeight>11in</PageHeight>
                    <MarginTop>0.25in</MarginTop>
                    <MarginLeft>0.25in</MarginLeft>
                    <MarginRight>0.25in</MarginRight>
                    <MarginBottom>0.25in</MarginBottom>
                  </DeviceInfo>";

            LocalReport report = new LocalReport();

            report.DataSources.Clear();
            report.EnableExternalImages = true;

            report.ReportPath = string.Format("{0}\\{1}", "Rdlc", report_name);

            DataTable dt = new DataTable();

            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["multiloansConnectionString"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT * FROM unisheets";

                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                }
            }
        report.DataSources.Add(new ReportDataSource("DataSet1", (DataTable) dt));

            return report.Render("PDF", deviceInfo, out mimeType,
                out encoding, out extension, out streamids, out warnings);
        }
}
}
