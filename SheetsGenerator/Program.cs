﻿using SheetsGenerator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SheetsGenerator.Sheets;
using MailService;

namespace SheetsGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            ISheet uniSheet = new UniSheet();
            var uniSheetReport = uniSheet.GenerateSheet();

            MailSender mailer = new MailSender();
            mailer.SendEmail(uniSheetReport);
        }
    }
}
